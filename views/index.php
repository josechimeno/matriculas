<?php 

require_once (__DIR__.'/../lib/controller/MatriculasControlles.php');
require_once (__DIR__.'/../lib/model/Matriculas.php');

$controller = new MatriculaControl();
$coches = $controller->generarMatriculas();
$i = 0;

?>

<html> 
	<head>
		<meta charset="utf-8">
		<title>Matrículas</title>
	</head>
	
	<body>
		<?php require_once(__DIR__.'/../lib/inc/header.php');?>
	<div id="wrapper">	
		<ul>
			<?php foreach($coches as $c){?>
				<li><a href="/details.php?pos=<?=$i?>"><?=$coches[$i]->getNumero()?>:<?=$coches[$i]->getMatriculas()?></a></li> 
			<?php $i++; } ?>
		</ul>
		
	</div>	
	</body>
</html>