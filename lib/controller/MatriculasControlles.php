<?php 

    require_once (__DIR__.'/../model/Matriculas.php');
    
    class MatriculaControl{
        
        public function generarMatriculas(){
            $coches = array();
            
            $matriculas = array("BBB","BBC","BBD");
            
            $colores = array("Azul","Verde","Rojo","Amarillo","Negro","Blanco");
            
            for($i = 0;$i<10000;$i++){
                $random1 = rand(0,2);
                $random2 = rand(0,5);
          
                 if($i<999){
                    $zerosdelante = str_pad($i, 4, "0", STR_PAD_LEFT);
                    array_push($coches, new Matriculas($zerosdelante,$matriculas[$random1],$colores[$random2]));
                    }else{
                    array_push($coches, new Matriculas($i,$matriculas[$random1],$colores[$random2]));
                    }
                }
            
           return $coches;
    
        }
        
        public function getMatricules($index){
            $listacoches = $this->generarMatriculas();
            return $listacoches[$index];
        }
    }

    
