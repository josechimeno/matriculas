<?php 

class Matriculas{
    
    private $_numero;
    private $_matriculas;
    private $_colores;
    
    public function __construct($n = 0, $m = "", $c = "") {
        $this->setNumero($n);
        $this->setMatriculas($m);
        $this->setColores($c);
    }
       
    /**
     * @return mixed
     */
    public function getNumero()
    {
        return $this->_numero;
    }

    /**
     * @return mixed
     */
    public function getMatriculas()
    {
        return $this->_matriculas;
    }

    /**
     * @return mixed
     */
    public function getColores()
    {
        return $this->_colores;
    }

    
    
    
    /**
     * @param mixed $_numero
     */
    public function setNumero($_numero)
    {
        $this->_numero = $_numero;
    }

    /**
     * @param mixed $_matriculas
     */
    public function setMatriculas($_matriculas)
    {
        $this->_matriculas = $_matriculas;
    }

    /**
     * @param mixed $_colores
     */
    public function setColores($_colores)
    {
        $this->_colores = $_colores;
    }

}

